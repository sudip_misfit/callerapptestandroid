package tech.misfit.callerapp;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tech.misfit.caller.utils.ContactModel;
import tech.misfit.callerapp.databinding.AdapterUsersBinding;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context context;
    OnItemClickListener onItemClickListener;
    OnViewClickListener onViewClickListener;

    List<ContactModel> userList;

    public UsersAdapter(Context context, List<ContactModel> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder((AdapterUsersBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_users, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        if (userList.get(position) != null) {
            String userName = TextUtils.isEmpty(userList.get(position).getContactName()) ? "No name found" : userList.get(position).getContactName();
            viewHolder.usersBinding.userName.setText(userName + "\n" + userList.get(position).getContactNumber());
        }

        /*if(userList.get(position).getIsRegistered()){
            viewHolder.usersBinding.audioCall.setVisibility(View.VISIBLE);
            viewHolder.usersBinding.videoCall.setVisibility(View.VISIBLE);
        } else {
            viewHolder.usersBinding.audioCall.setVisibility(View.GONE);
            viewHolder.usersBinding.videoCall.setVisibility(View.GONE);
        }*/

        viewHolder.usersBinding.audioCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClicked(position, userList.get(position), true);
            }
        });

        viewHolder.usersBinding.videoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClicked(position, userList.get(position), false);
            }
        });

        viewHolder.usersBinding.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewClickListener.onViewClicked(position, userList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public interface OnItemClickListener {
        void onItemClicked(int position, ContactModel user, boolean isAudioCall);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnViewClickListener {
        void onViewClicked(int position, ContactModel user);
    }

    public void setOnViewClickListener(OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AdapterUsersBinding usersBinding;

        public ViewHolder(AdapterUsersBinding usersBinding) {
            super(usersBinding.getRoot());
            this.usersBinding = usersBinding;
        }
    }


}


