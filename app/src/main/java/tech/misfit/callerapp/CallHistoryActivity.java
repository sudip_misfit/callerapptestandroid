package tech.misfit.callerapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import tech.misfit.caller.manager.CallHistoryListener;
import tech.misfit.caller.manager.UserManager;
import tech.misfit.caller.model.response.CallHistoryResponse;
import tech.misfit.caller.model.response.Error;

public class CallHistoryActivity extends AppCompatActivity implements CallHistoryListener {

    UserManager userManager;
    RecyclerView recyclerView;
    CallHistoryAdapter callHistoryAdapter;

    CallHistoryResponse callHistoryResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_history);

        recyclerView = findViewById(R.id.rvUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == callHistoryAdapter.getItemCount() - 1) {
                    //bottom of list!
                    if (callHistoryResponse.getCurrentPage()<callHistoryResponse.getTotalPage()) {
                        userManager.getCallHistory(callHistoryResponse.getCurrentPage()+1, CallHistoryActivity.this);
                    }
                }
            }
        });

        callHistoryAdapter = new CallHistoryAdapter(this, new ArrayList<>());
        recyclerView.setAdapter(callHistoryAdapter);

        userManager = new UserManager(this);
        userManager.getCallHistory(1, this);

    }

    @Override
    public void onSuccess(CallHistoryResponse callHistory) {
        callHistoryResponse = callHistory;
        callHistoryAdapter.addData(callHistory.getData());
    }

    @Override
    public void onFailed(Error error) {

    }

}
