package tech.misfit.callerapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import tech.misfit.caller.manager.ContactFetchListener;
import tech.misfit.caller.manager.ContactSyncListener;
import tech.misfit.caller.manager.OutgoingCallListener;
import tech.misfit.caller.manager.OutgoingCallManager;
import tech.misfit.caller.manager.RegistrationListener;
import tech.misfit.caller.manager.UserManager;
import tech.misfit.caller.model.request.UserParams;
import tech.misfit.caller.model.response.Error;
import tech.misfit.caller.utils.ContactModel;
import tech.misfit.caller.utils.LocalStorage;
import tech.misfit.caller.utils.PermissionListener;

public class RegisterActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView currentUser;
    EditText etUserName;
    EditText etUserPhoneNumber;
    Button btnRegister;

    UserManager userManager;
    OutgoingCallManager outgoingCallManager;

    private PermissionListener permissionListener;
    public static final String TOKEN = "86e422a629ab46caf79270fcd52ef4de3fae2121d72e4e74d48c046b0ea76c26";
    private static final int reqCode = 580;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_register);
        String fcmToken = LocalStorage.getInstance().getStringData(RegisterActivity.this, "fcm_token");
        Log.e("FCMToken", "fcmToken: " + fcmToken);

        currentUser = findViewById(R.id.tvCurrentUser);
        etUserName = findViewById(R.id.etName);
        etUserPhoneNumber = findViewById(R.id.etPhoneNumber);
        btnRegister = findViewById(R.id.btnRegister);

        recyclerView = findViewById(R.id.rvUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        userManager = new UserManager(this);
        outgoingCallManager = new OutgoingCallManager(this);

        if (!TextUtils.isEmpty(LocalStorage.getInstance().getMyInformation(this))) {
            Log.e("Socket", "my info: " + LocalStorage.getInstance().getMyInformation(this));
            UserParams myInfo = new Gson().fromJson(LocalStorage.getInstance().getMyInformation(this), UserParams.class);
            userManager.register(TOKEN, myInfo.getId(), myInfo.getName(), myInfo.getFcmToken(), myInfo.getDeviceType(), "https://picsum.photos/200/300", new RegistrationListener() {
                @Override
                public void onSuccess(String data) {
                    currentUser.setText(String.format("You are: %s | %s", myInfo.getName(), myInfo.getId()));
                    getContacts();
                }

                @Override
                public void onFailed(Error error) {

                }
            });
        }

        btnRegister.setOnClickListener(view -> {
            if(TextUtils.isEmpty(etUserName.getText().toString())){
                Toast.makeText(RegisterActivity.this, "Enter your name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(etUserPhoneNumber.getText().toString())){
                Toast.makeText(RegisterActivity.this, "Enter your number", Toast.LENGTH_SHORT).show();
            } else {
                userManager.register(TOKEN, etUserPhoneNumber.getText().toString(), etUserName.getText().toString(), fcmToken, "android", "https://picsum.photos/200/300", new RegistrationListener() {
                    @Override
                    public void onSuccess(String data) {
                        currentUser.setText(String.format("You are: %s | %s", etUserName.getText().toString(), etUserPhoneNumber.getText().toString()));
                        getContacts();
                    }

                    @Override
                    public void onFailed(Error error) {
                        Log.e("RegisterActivity", "Error: "+ new Gson().toJson(error));
                    }
                });
            }

        });

        findViewById(R.id.btnCallHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, CallHistoryActivity.class));
            }
        });


    }

    private void getUsers(List<ContactModel> users) {

        UsersAdapter usersAdapter = new UsersAdapter(RegisterActivity.this, users);
        usersAdapter.setOnItemClickListener(new UsersAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position, ContactModel user, boolean isAudioCall) {
                outgoingCallManager.makeOutgoingCall(user.getContactName(), user.getContactNumber(), "https://picsum.photos/200", isAudioCall, new OutgoingCallListener() {

                    @Override
                    public void onSuccess(int code, ContactModel contact) {
                        Log.e("OutgoingCallManager", "code: " + code + "; contact: " + new Gson().toJson(contact));
                    }

                    @Override
                    public void onFailed(Error error) {
                        Log.e("OutgoingCallManager", "error: " + new Gson().toJson(error));
                    }
                });
            }
        });
        /*usersAdapter.setOnViewClickListener(new UsersAdapter.OnViewClickListener() {
            @Override
            public void onViewClicked(int position, ContactModel user) {
                String fcmToken = LocalStorage.getInstance().getFirebaseToken(RegisterActivity.this);
                userManager.register(TOKEN, user.getContactNumber(), user.getContactName(), fcmToken, "android", "https://picsum.photos/200/300", new RegistrationListener() {
                    @Override
                    public void onSuccess(String data) {
                        currentUser.setText(String.format("You are: %s | %s", user.getContactName(), user.getContactNumber()));
                        getContacts();
                    }

                    @Override
                    public void onFailed(Error error) {

                    }
                });
            }
        });*/
        recyclerView.setAdapter(usersAdapter);
        usersAdapter.notifyDataSetChanged();
    }

    public void checkPermission(String[] permission, PermissionListener permissionListener) {
        try {
            this.permissionListener = permissionListener;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                boolean allGranted = true;
                int countPermission = 0;
                Log.e("permission_list", String.valueOf(permission.length));
                while (permission.length > countPermission) {
                    if ((ContextCompat.checkSelfPermission(this, permission[countPermission]) !=
                            PackageManager.PERMISSION_GRANTED)) {
                        allGranted = false;
                        break;
                    }
                    countPermission++;
                }
                if (!allGranted) ActivityCompat.requestPermissions(this, permission, reqCode);
                else this.permissionListener.permissionGranted();

            } else this.permissionListener.permissionGranted();

        } catch (Exception e) {
            this.permissionListener.permissionDenied(-1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        try {
            switch (requestCode) {
                case reqCode: {
                    if (permissions.length > 0) {
                        boolean allGranted = true;
                        int count = 0;
                        while (permissions.length > count) {
                            if (ContextCompat.checkSelfPermission(this, permissions[count]) !=
                                    PackageManager.PERMISSION_GRANTED) {
                                allGranted = false;
                                break;
                            }
                            count++;
                        }
                        if (allGranted) this.permissionListener.permissionGranted();
                        else this.permissionListener.permissionDenied(count);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void getContacts(){
        checkPermission(new String[]{Manifest.permission.READ_CONTACTS}, new PermissionListener() {
            @Override
            public void permissionGranted() {
                userManager.syncContact(new ContactSyncListener() {

                    @Override
                    public void onSuccess(String data) {
                        userManager.fetchActiveContacts(new ContactFetchListener() {

                            @Override
                            public void onSuccess(ArrayList<ContactModel> activeContacts) {
                                populateListView(activeContacts);
                            }

                            @Override
                            public void onFailed(Error error) {

                            }
                        });
                    }

                    @Override
                    public void onFailed(Error error) {

                    }
                });
            }

            @Override
            public void permissionDenied(int position) {

            }

        });
    }

    private void populateListView(ArrayList<ContactModel> activeContacts) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getUsers(activeContacts);
            }
        });
    }
}
