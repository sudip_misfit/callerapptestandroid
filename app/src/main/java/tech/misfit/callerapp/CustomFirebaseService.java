package tech.misfit.callerapp;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import tech.misfit.caller.manager.IncomingCallListener;
import tech.misfit.caller.manager.IncomingCallManager;
import tech.misfit.caller.model.response.Error;
import tech.misfit.caller.utils.ContactModel;
import tech.misfit.caller.utils.LocalStorage;


public class CustomFirebaseService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NonNull String fcmToken) {
        super.onNewToken(fcmToken);
        Log.e("CustomFirebaseService", "token: " + fcmToken);
        LocalStorage.getInstance().setFirebaseToken(this, fcmToken);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("CustomFirebaseService", "notification received: ");
        new IncomingCallManager(this).handleIncomingCall(remoteMessage, new IncomingCallListener() {

            @Override
            public void onSuccess(int code, ContactModel contact) {
                Log.e("IncomingCallManager", "code: " + code + "; contact: " + new Gson().toJson(contact));
            }

            @Override
            public void onFailed(Error error) {
                Log.e("IncomingCallManager", "error: " + new Gson().toJson(error));
            }
        });
    }

}
