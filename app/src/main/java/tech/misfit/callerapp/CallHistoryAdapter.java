package tech.misfit.callerapp;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tech.misfit.caller.model.response.CallHistoryResponse;
import tech.misfit.caller.utils.ContactManager;
import tech.misfit.caller.utils.ContactModel;
import tech.misfit.callerapp.databinding.AdapterCallHistoryBinding;

public class CallHistoryAdapter extends RecyclerView.Adapter<CallHistoryAdapter.ViewHolder> {

    private Context context;
    OnItemClickListener onItemClickListener;
    OnViewClickListener onViewClickListener;

    List<CallHistoryResponse.Data> callHistoryList;

    public CallHistoryAdapter(Context context, List<CallHistoryResponse.Data> callHistoryList) {
        this.context = context;
        this.callHistoryList = callHistoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder((AdapterCallHistoryBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_call_history, parent, false));
    }

    public void addData(List<CallHistoryResponse.Data> callHistoryList){
        this.callHistoryList.addAll(callHistoryList);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        String name = getUsername(callHistoryList.get(position).getUser());
        String duration = getCallDuration(callHistoryList.get(position).getDuration());
        String callType = getCallType(callHistoryList.get(position).getType(), callHistoryList.get(position).getStatus());
        String relativeTime = getRelativeTime(callHistoryList.get(position).getTime());
        viewHolder.callHistoryBinding.userName.setText(
                        "User: " + name +
                        "\nDuration: " + duration +
                        "\nCall type: " + callType +
                        "\nTime: " + relativeTime);


    }

    private String getUsername(String user) {
        return TextUtils.isEmpty(ContactManager.getContactNameByNumber(user, context)) ? user : ContactManager.getContactNameByNumber(user, context);
    }

    private String getCallDuration(Long duration) {
        return DateUtils.formatElapsedTime(duration);
    }

    private String getRelativeTime(String time) {
        return String.valueOf(DateUtils.getRelativeTimeSpanString(Long.parseLong(time) * 1000, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
    }

    private String getCallType(Integer type, Integer status) {
        String callType = "";
        if(type == 0){
            callType = "Incoming | ";
            if(status==0){
                callType = callType + "Missed call";
            } else {
                callType = callType + "Connected call";
            }
        } else if(type == 1){
            callType = "Outgoing | ";
            if(status==0){
                callType = callType + "Not answered call";
            } else {
                callType = callType + "Connected call";
            }
        }
        return callType;
    }

    @Override
    public int getItemCount() {
        return callHistoryList.size();
    }

    public interface OnItemClickListener {
        void onItemClicked(int position, ContactModel user, boolean isAudioCall);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnViewClickListener {
        void onViewClicked(int position, ContactModel user);
    }

    public void setOnViewClickListener(OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        AdapterCallHistoryBinding callHistoryBinding;

        public ViewHolder(AdapterCallHistoryBinding callHistoryBinding) {
            super(callHistoryBinding.getRoot());
            this.callHistoryBinding = callHistoryBinding;
        }
    }


}


